import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static java.lang.Thread.sleep;
import static org.openqa.selenium.Keys.ENTER;

public class  User {
    WebDriver driver;

    @BeforeClass
    public static void setup(){
        System.setProperty("webdriver.gecko.driver","C:\\drivers\\geckodriver.exe");
    }

    @Before
    public void precondition (){
        driver = new FirefoxDriver();
    }

    @Test
    public void openUserData() throws InterruptedException {
        //registration
        driver.get("https://user-data.hillel.it/html/registration.html");

            driver.findElement(By.cssSelector(".registration")).click();
            driver.findElement(By.id("first_name")).sendKeys("1olga");
            driver.findElement(By.id("last_name")).sendKeys("1olga");
            driver.findElement(By.id("field_work_phone")).sendKeys("1234567");
            driver.findElement(By.id("field_phone")).sendKeys("380509879562");
            driver.findElement(By.id("field_email")).sendKeys("olga@olgaq.ru");
            driver.findElement(By.id("field_password")).sendKeys("Qwertyu12");
            driver.findElement(By.id("male")).click();
            driver.findElement(By.id("position")).click();
            driver.findElement(By.cssSelector("[value=manager]")).click();
            driver.findElement(By.cssSelector(".create_account")).click();

        Thread.sleep(1000);
        driver.navigate().refresh();
        Thread.sleep(1000);

        //login
        driver.findElement(By.id("email")).sendKeys("olga@olgaq.ru");
        driver.findElement(By.id("password")).sendKeys("Qwertyu12");
        Thread.sleep(1000);
        driver.findElement(By.cssSelector(".login_button")).click();
        Thread.sleep(1000);
        //search of user
        driver.findElement(By.id("employees")).click();
        driver.findElement(By.id("first_name")).click();
        driver.findElement(By.id("first_name")).sendKeys("1olga");
        driver.findElement(By.id("last_name")).click();
        driver.findElement(By.id("last_name")).sendKeys("1olga");
        driver.findElement(By.id("mobile_phone")).click();
        driver.findElement(By.id("mobile_phone")).sendKeys("380509879562");
        driver.findElement(By.id("search")).click();

        }

    @After
    public void postCondition(){
      //  driver.quit();
    }


}
